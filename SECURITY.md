# Security Policy

## Reporting a Vulnerability

In case you have discovered a vulnerability, please reach out privately via email to pczerkas@gmail.com instead of opening an issue.
